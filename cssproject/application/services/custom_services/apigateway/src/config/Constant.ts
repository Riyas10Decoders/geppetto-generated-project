export const SECURITYURL = 'http://cssproject-app.cssproject.svc.cluster.local:3007';
export const AUTHPROXYURL = 'http://cssproject-app.cssproject.svc.cluster.local:3009';
export const ADMINURL = 'http://cssproject-app.cssproject.svc.cluster.local:3010';
export const CAMUNDAURL = 'http://cssproject-app.cssproject.svc.cluster.local:3008';
export const TESTFETUREURL = 'http://cssproject-app.cssproject.svc.cluster.local:8002';